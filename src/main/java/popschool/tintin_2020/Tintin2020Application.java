package popschool.tintin_2020;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import popschool.tintin_2020.dao.PersonnageInterface;

@SpringBootApplication
public class Tintin2020Application {

    public static void main(String[] args) {
        SpringApplication.run(Tintin2020Application.class, args);
    }

    @Autowired
    PersonnageInterface personnageInterface;


    public void run(String... args) throws Exception {

        personnageInterface.listAlbumsOfPersonnage().entrySet().forEach(System.out::println);

    }

}
