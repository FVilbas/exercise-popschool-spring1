package popschool.tintin_2020.model;

import lombok.Data;

@Data
public class Personnage {
    private int id;
    private String nom;
    private String prenom;
    private String profession;
    private String sexe;
    private String genre;

    public Personnage(int id, String nom, String prenom, String profession, String sexe, String genre) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.profession = profession;
        this.sexe = sexe;
        this.genre = genre;
    }

    public Personnage(String nom, String prenom, String profession, String sexe, String genre) {
        this.nom = nom;
        this.prenom = prenom;
        this.profession = profession;
        this.sexe = sexe;
        this.genre = genre;
    }
}
