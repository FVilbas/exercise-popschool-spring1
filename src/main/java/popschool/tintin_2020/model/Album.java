package popschool.tintin_2020.model;

import lombok.Data;

@Data
public class Album {

    private int id;
    private String titre;
    private int annee;
    private int suivant;

    public Album(int id, String titre, int annee, int suivant) {
        this.id = id;
        this.titre = titre;
        this.annee = annee;
        this.suivant = suivant;
    }
}
