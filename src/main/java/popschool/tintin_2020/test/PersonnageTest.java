package popschool.tintin_2020.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import popschool.tintin_2020.dao.AlbumInterface;
import popschool.tintin_2020.dao.PersonnageDAO;
import popschool.tintin_2020.dao.PersonnageInterface;
import popschool.tintin_2020.model.Album;
import popschool.tintin_2020.model.Personnage;

import java.util.List;

@Component
public class PersonnageTest implements CommandLineRunner {

    @Autowired
    PersonnageInterface personnageInterface;

    @Autowired
    @Qualifier("albumNamedParameters")
    AlbumInterface albumInterface;


    @Override
    public void run(String... args) throws Exception {

        albumInterface.listPersonnagesOfAlbum().entrySet().forEach(System.out::println);

    }
}
