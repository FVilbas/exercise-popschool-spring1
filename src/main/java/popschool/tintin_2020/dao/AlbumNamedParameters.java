package popschool.tintin_2020.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import popschool.tintin_2020.model.Album;
import popschool.tintin_2020.model.Personnage;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class AlbumNamedParameters extends AlbumDAO{

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    String sql_base ="SELECT id, titre, annee, suivant FROM albums";

    @Override
    public List<Album> listAlbums() {
        return namedParameterJdbcTemplate.query(sql_base,super::rowAlbums);
    }

    @Override
    public Optional<Album> findById(int id) {
        return namedParameterJdbcTemplate.query(sql_base + " where id= :id", new MapSqlParameterSource("id", id),super::optionalAlbum);
    }

    @Override
    public List<Album> listByTitre(String titre) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("titre","%"+titre+"%");
        return namedParameterJdbcTemplate.query(sql_base+" where titre like :titre",mapSqlParameterSource,super::rowAlbums);
    }

    @Override
    public List<Album> listByParution(int an1, int an2) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("annee1",an1);
        mapSqlParameterSource.addValue("annee2",an2);

        return namedParameterJdbcTemplate.query(sql_base+" where annee >= :annee1 and annee <= :annee2",mapSqlParameterSource,super::rowAlbums);
    }

    @Override
    public Map<Album, List<Personnage>> listPersonnagesOfAlbum() {

        return namedParameterJdbcTemplate.query("SELECT p.id, p.nom, p.prenom, p.profession, p.sexe, p.genre," +
                " alb.id, alb.titre, alb.annee, alb.suivant" +
                " FROM personnages p inner join apparaits a on p.id = a.personnage_id" +
                " inner join albums alb on a.album_id = alb.id",this::extractAlbumPers);
    }




}
