package popschool.tintin_2020.dao;

import popschool.tintin_2020.model.Album;
import popschool.tintin_2020.model.Personnage;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface AlbumInterface {

    List<Album> listAlbums();
    Optional<Album> findById(int id);    List<Album> listByTitre(String titre);
    List<Album> listByParution(int an1, int an2);
    Map<Album, List<Personnage>> listPersonnagesOfAlbum();
    int ajoutAlbum(Album album);



}
