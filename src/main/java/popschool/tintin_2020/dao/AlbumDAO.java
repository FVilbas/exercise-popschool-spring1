package popschool.tintin_2020.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import popschool.tintin_2020.model.Album;
import popschool.tintin_2020.model.Personnage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class AlbumDAO implements AlbumInterface{

    @Autowired
    JdbcTemplate jdbcTemplate;

    String sql_base ="SELECT id, titre, annee, suivant FROM albums";

    String sql_findsuivant= "SELECT a.id, a.titre, a.annee, if null(b.titre,'sans suite') " +
            "as titre_suivant from album a lest outer join album b on a.suivant = b.id";

    @Override
    public List<Album> listAlbums() {
        return jdbcTemplate.query(sql_base,this::rowAlbums);
    }

    @Override
    public Optional<Album> findById(int id) {
        return jdbcTemplate.query(sql_base +" where id= ?", this::optionalAlbum,id);
    }

    @Override
    public List<Album> listByTitre(String titre) {
        titre = "%"+titre+"%";
        return jdbcTemplate.query(sql_base+" where titre like ?",this::rowAlbums,titre);
    }

    @Override
    public List<Album> listByParution(int an1, int an2) {
        return jdbcTemplate.query(sql_base+" where annee >= ? and annee <= ?",this::rowAlbums,an1,an2);
    }

    @Override
    public Map<Album, List<Personnage>> listPersonnagesOfAlbum() {

        return jdbcTemplate.query("SELECT p.id, p.nom, p.prenom, p.profession, p.sexe, p.genre," +
                " alb.id, alb.titre, alb.annee, alb.suivant" +
                " FROM personnages p inner join apparaits a on p.id = a.personnage_id" +
                " inner join albums alb on a.album_id = alb.id",this::extractAlbumPers);
    }

    Map<Album, List<Personnage>> extractAlbumPers(ResultSet rs) throws SQLException {
        Map<Album, List<Personnage>> map = new HashMap<>();
        while (rs.next()) {
            Personnage valeur = new Personnage(rs.getInt(1),rs.getString(2),rs.getString(3)
                    ,rs.getString(4),rs.getString(5),rs.getString(6));
            Album cle = new Album(rs.getInt(7),rs.getString(8),rs.getInt(9),rs.getInt(10));
            if (map.containsKey(cle)) {
                map.get(cle).add(valeur);
            } else {
                List<Personnage> list = new ArrayList<>();
                list.add(valeur);
                map.put(cle, list);
            }
        }
        return map;
    }



    @Override
    public int ajoutAlbum(Album album) {
        return jdbcTemplate.update("insert into albums (id, titre, annee, suivant) values (?,?,?,?)",album.getId(),album.getTitre(),album.getAnnee(),album.getSuivant());

    }

    protected Album rowAlbums(ResultSet rs, int rowNum) throws SQLException {
        return (new Album(rs.getInt("id"),rs.getString("titre"),rs.getInt("annee"),rs.getInt("suivant")));}

    protected Optional<Album> optionalAlbum(ResultSet rs) throws SQLException {
        if(rs.next()){
            return Optional.of(new Album(rs.getInt("id"),rs.getString("titre"),rs.getInt("annee"),rs.getInt("suivant")));}
        return Optional.ofNullable(null);
    }


}
