package popschool.tintin_2020.dao;

import popschool.tintin_2020.model.Album;
import popschool.tintin_2020.model.Personnage;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface PersonnageInterface {

    List<Personnage> findAllPersonnages();
    Optional<Personnage> findById(int id);

    Optional<Personnage> findByNomPrenom(String nom); //args null possible
    List<Personnage> listPersonnagesBySexe(String sexe); //référence de méthode
    List<Personnage> listPersonnagesByJob(String job); //lamba
    List<Personnage> listPersonnagesByGenre(String genre); //RowMapper
    Map<Personnage, List<Album>> listAlbumsOfPersonnage();
    int ajoutPersonnage(Personnage personnage);

}
