package popschool.tintin_2020.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import popschool.tintin_2020.model.Album;
import popschool.tintin_2020.model.Personnage;

import java.sql.JDBCType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class PersonnageDAO implements PersonnageInterface{

    @Autowired
    JdbcTemplate jdbcTemplate;


    @Override
    public List<Personnage> findAllPersonnages() {
        List<Personnage> list = jdbcTemplate.query("SELECT id, nom, prenom, profession, sexe, genre FROM personnages", (rs, rowNum)-> new Personnage(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("profession"),rs.getString("sexe"),rs.getString("genre")));
    return list;
    }

    @Override
    public Optional<Personnage> findById(int id) {
        return jdbcTemplate.query("SELECT id, nom, prenom, profession, sexe, genre FROM personnages where id= ?",this::mapOptionalPersonnage,id);
    }

    @Override
    public Optional<Personnage> findByNomPrenom(String nom) {
        String sql="SELECT id, nom, prenom, profession, sexe, genre FROM personnages WHERE nom like ? or prenom like ?";

        Optional<Personnage> pers = jdbcTemplate.query(sql,this::mapOptionalPersonnage,nom,nom);

        return pers;

    }

    @Override
    public List<Personnage> listPersonnagesBySexe(String sexe) {
        String sql = "SELECT id, nom, prenom, profession, sexe, genre FROM personnages where sexe = ? ";

        String s = sexe.toUpperCase();

        return jdbcTemplate.query(sql,this::mapPersonnage,s);


    }

    @Override
    public List<Personnage> listPersonnagesByJob(String job) {
        return jdbcTemplate.query("SELECT id, nom, prenom, profession, sexe, genre FROM personnages where profession = ? ",
                (rs, rowNum)->new Personnage(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom")
                        ,rs.getString("profession"),rs.getString("sexe"),rs.getString("genre")),job);
    }

    @Override
    public List<Personnage> listPersonnagesByGenre(String genre) {
        String sql = "SELECT id, nom, prenom, profession, sexe, genre FROM personnages where genre = ?";
        genre= genre.toUpperCase();

        PersonnageRowMapper rowMapper = new PersonnageRowMapper();

        return jdbcTemplate.query(sql, rowMapper, genre);
    }

    @Override
    public Map<Personnage, List<Album>> listAlbumsOfPersonnage() {
        String sql = "SELECT p.id, p.nom, p.prenom, p.profession, p.sexe, p.genre, alb.id, alb.titre, alb.annee, alb.suivant FROM personnages p inner join apparaits a on p.id = a.personnage_id inner join albums alb on a.album_id = alb.id";

        return jdbcTemplate.query(sql,this::extractPersAlbum);
    }

    Map<Personnage, List<Album>> extractPersAlbum(ResultSet rs) throws SQLException {
        Map<Personnage, List<Album>> map = new HashMap<>();
        while (rs.next()) {
            Personnage cle = new Personnage(rs.getInt(1),rs.getString(2),rs.getString(3)
                    ,rs.getString(4),rs.getString(5),rs.getString(6));
            Album valeur = new Album(rs.getInt(7),rs.getString(8),rs.getInt(9),rs.getInt(10));
            if (map.containsKey(cle)) {
                map.get(cle).add(valeur);
            } else {
                List<Album> list = new ArrayList<>();
                list.add(valeur);
                map.put(cle, list);
            }
        }
        return map;
    }

        @Override
    public int ajoutPersonnage(Personnage personnage) {
            return jdbcTemplate.update("insert into personnages (nom, prenom, profession, sexe,genre) values (?,?,?,?,?)",personnage.getNom(),personnage.getPrenom(),personnage.getProfession(),personnage.getSexe(),personnage.getGenre());
    }

    protected Optional<Personnage> mapOptionalPersonnage(ResultSet rs) throws SQLException {
        if(rs.next()){
            return Optional.of(new Personnage(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("profession"),rs.getString("sexe"),rs.getString("genre")));}
        return Optional.ofNullable(null);
    }

    protected Personnage mapPersonnage(ResultSet rs, int rowNum) throws SQLException {
            return (new Personnage(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("profession"),rs.getString("sexe"),rs.getString("genre")));}


    private static final class PersonnageRowMapper implements RowMapper<Personnage> {
        @Override
        public Personnage mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Personnage(rs.getInt("id"),rs.getString("nom"),rs.getString("prenom"),rs.getString("profession"),rs.getString("sexe"),rs.getString("genre"));
        }
    }

}



